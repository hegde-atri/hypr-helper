# hypr-helper 
[![Licence](https://img.shields.io/github/license/hegde-atri/hyprland-workspaces?color=red)](https://github.com/hegde-atri/hyprland-workspaces/blob/main/LICENCE)
[![Version](https://img.shields.io/crates/v/hypr-helper?color=9cf)](https://crates.io/crates/hypr-helper/versions)
[![Crates.io Total Downloads](https://img.shields.io/crates/d/hyprland-workspaces?label=hyprland-workspaces%20(deprecated)%20downloads)](https://crates.io/crates/hyprland-workspaces)
[![Crates.io Total Downloads](https://img.shields.io/crates/d/hypr-helper?label=hypr-helper%20downloads&color=green)](https://crates.io/crates/hypr-helper)
![GitLab all issues](https://img.shields.io/gitlab/issues/all/hegde-atri%2Fhypr-helper)

An application that listens to the Hyprland UNIX socket for changes and returns chosen properties (example: workspace information, active window name).

# Prerequisites

You should be running Hyprland.

# Why

Writing this helped me cut down RAM usage for getting workspace information by almost 600%! (12MB -> 2.4MB) and of course barely any CPU usage. 
This CLI tool will also come with some extra features, such as dynamically updating the gaps

# Roadmap

- [x] Workspace information.
- [x] Active window name.
- [x] Resize gaps (increase, decrease and reset).
- [ ] Manage monitor configuration.

# How to install

You can install this by `cargo` or building the project yourself

``` sh
# Using cargo
cargo install hypr-helper
```

```sh
# Cloning and building
git clone https://gitlab.com/hegde-atri/hypr-helper
cd hyprland-workspaces
cargo build --release
```

