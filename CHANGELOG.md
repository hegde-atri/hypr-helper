# 0.1.0

- You can listen to the workspace information using `hypr-helper workspaces`
- You can listen to the active window name using `hypr-helper windowname`
- You can now control gaps using `hypr-helper gaps`.
  - You can increase, decrease and reset the gaps.

# 0.1.1

- You can no longer decrease gap size if you current gap size is 0 or less.
- Added missing docs.
- Updated README with relevant instructions.
