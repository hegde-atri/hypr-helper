use clap::{Parser, Subcommand};

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
pub struct HyprHelperArgs {
    /// Describe data to get.
    #[clap(subcommand)]
    pub action: Action,
}

#[derive(Debug, Subcommand)]
pub enum Action {
    /// Returns workspace info in json format {id, status, monitor}.
    ///
    /// id is the workspace number.
    ///
    /// The status of each workspace is such that:
    /// 0 -> empty,
    /// 1 -> has windows,
    /// 2 -> active workspace.
    ///
    /// Monitors is currently not supported and will always return 1.
    Workspaces,
    /// Returns active window name as a string.
    #[clap(subcommand)]
    Windowname(WindownameVariant),
    /// Modify gaps
    #[clap(subcommand)]
    Gaps(GapsVariant),
}

/// The variant of the window name.
///
/// - `Title`: The title of the application
///         Example: "Firefox"
/// - `App`: The title given by the application
///         Example: "Odysee"
/// - `Full`: Title and App
///         Example: "Firefox,Odysee"
#[derive(Debug, Subcommand)]
pub enum WindownameVariant {
    /// Just the title of the active application.
    ///
    ///
    /// Might be active tab name from firefox
    /// or current directory in the terminal
    /// Example:
    /// Odysee
    /// ~/Downloads
    Title,
    /// Just the active application
    ///
    /// Example:
    /// Firefox
    /// Foot
    /// Doom Emacs
    App,
    /// Return in format app, title
    ///
    /// Example:
    /// Foot, ~/Downloads
    /// Firefox, Odysee
    Full,
}

/// The gaps command you want to send.
///
/// - `Increase`: Increase the gap size
/// - `Decrease`: Decrease the gap size
/// - `Reset`: Reset the gap size
#[derive(Debug, Subcommand)]
pub enum GapsVariant {
    /// Increase gap size
    Increase,
    /// Decrease gap size
    Decrease,
    /// Reset gap size to 0
    Reset,
}
