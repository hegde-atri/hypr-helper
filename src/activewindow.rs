use crate::{args::WindownameVariant, utils::get_reader};
use std::io::BufRead;

/// Prints the active window name
///
/// This listens to the UNIX socket for events, when your
/// window focus changes, it updates prints out the name
/// of the active window.
pub fn get_activewindow_name(variant: WindownameVariant) {
    let input = get_reader();
    for line in input.lines() {
        match line {
            Ok(line) => {
                let mut line_iterator = line.splitn(2, ">>");
                let verb = line_iterator.next().unwrap();
                if let Some(info) = line_iterator.next() {
                    if verb == "activewindow" {
                        match variant {
                            WindownameVariant::App => {
                                if let Some(app) = info.split(',').skip(1).next() {
                                    println!("{app}");
                                } else {
                                    invalid_hyprland_version();
                                }
                            }
                            WindownameVariant::Title => {
                                if let Some(title) = info.split(',').next() {
                                    println!("{title}");
                                } else {
                                    dbg!(&info);
                                    invalid_hyprland_version();
                                }
                            }
                            WindownameVariant::Full => {
                                println!("{info}");
                            }
                        }
                    }
                } else {
                    invalid_hyprland_version();
                }
            }
            Err(_) => {}
        }
    }
}

/// Helper function which prints Incompatible Hyprland version and exits the process.
fn invalid_hyprland_version() {
    println!("Incompatible Hyprland version");
    std::process::exit(1);
}
