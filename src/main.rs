#![warn(missing_docs)]

//! # hypr-helper
//!
//!
//!
//! - [x] Workspace information.
//! - [x] Active window name.
//! - [x] Resize gaps (increase, decrease and reset).
//! - [ ] Manage monitor configuration.

use activewindow::get_activewindow_name;
use args::HyprHelperArgs;
use clap::Parser;
use gaps::handle_gaps_command;
use workspace::print_workspaces;
mod activewindow;
mod args;
mod gaps;
mod utils;
mod workspace;

fn main() {
    let args = HyprHelperArgs::parse();

    match args.action {
        args::Action::Windowname(variant) => get_activewindow_name(variant),
        args::Action::Workspaces => print_workspaces(),
        args::Action::Gaps(variant) => handle_gaps_command(variant),
    }
}
